{-# LANGUAGE ConstraintKinds       #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE KindSignatures        #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE FlexibleInstances     #-} {-# LANGUAGE UndecidableInstances  #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Existential where

import           Data.Typeable
import Data.Kind (Constraint, Type)
import Control.Monad.State
import qualified Data.Map as M


data Any where
  Any :: a -> Any


elimAny :: (forall a. a -> r) -> Any -> r
elimAny f (Any a) = f a




data HasShow where
  HasShow :: Show t => t -> HasShow

elimHasShow :: (forall a. Show a => a -> r) -> HasShow -> r
elimHasShow f (HasShow a) = f a

instance Show HasShow where
  show = elimHasShow $ ("HasShow " <>) . show


data Dynamic where
  Dynamic :: Typeable t => t -> Dynamic

elimDynamic :: (forall a. Typeable a => a -> r) -> Dynamic -> r
elimDynamic f (Dynamic a) = f a

fromDynamic :: Typeable a => Dynamic -> Maybe a
fromDynamic = elimDynamic cast


data Has (c :: Type -> Constraint) where
  Has :: c t => t -> Has c

