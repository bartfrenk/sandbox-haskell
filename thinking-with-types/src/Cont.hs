{-# LANGUAGE RankNTypes #-}
module Cont where


newtype Cont a = Cont {
  unCont :: forall r. (a -> r) -> r
}


instance Functor Cont where
  f `fmap` (Cont k) = Cont $ \g -> k (g . f)


instance Applicative Cont where
  pure a = Cont $ \g -> g a
  (Cont f) <*> (Cont a) = Cont $ \g -> f (\k -> a (g . k))


instance Monad Cont where
  -- g :: (b -> r)
  -- k :: a -> (b -> r) -> r
  -- fs :: (a -> r) -> r
  -- Basic idea: Give fs a function a -> r, and use g and k to construct this function.
  (Cont fs) >>= k = Cont $ \g -> fs (\a -> unCont (k a) g)


runCont :: (forall r. (a -> r) -> r) -> a
runCont f = f id

withVersionNumber :: (Double -> r) -> r
withVersionNumber f = f 1.0

withTimestamp :: (Int -> r) -> r
withTimestamp f = f 1532083362

withOS :: (String -> r) -> r
withOS f = f "linux"

releaseString :: String
releaseString =
  withVersionNumber $ \version ->
    withTimestamp $ \date ->
      withOS $ \os ->
        os ++ "-" ++ show version ++ "-" ++ show date

releaseStringCont :: String
releaseStringCont = runCont $ unCont $ do
  version <- Cont withVersionNumber
  date <- Cont withTimestamp
  os <- Cont withOS
  pure $ os ++ "-" ++ show version ++ "-" ++ show date

newtype ContT m a = ContT { unContT :: m (Cont a) }

instance Functor m => Functor (ContT m) where
  f `fmap` (ContT ma) = ContT $ fmap f `fmap` ma

instance Applicative m => Applicative (ContT m) where
  pure a = ContT $ pure $ pure a
  (ContT fs) <*> (ContT ma) = ContT $ fmap (<*>) fs <*> ma

instance Monad m => Monad (ContT m) where
  (ContT ma) >>= f = do
    let gs g = unContT (unCont g f) -- use the specific structure of Cont
    ContT $ ma >>= gs
