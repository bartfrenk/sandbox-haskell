{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE UndecidableInstances  #-}
{-# LANGUAGE ConstraintKinds       #-}
{-# LANGUAGE GADTs                 #-}
module RepoExistential where

import Data.Kind


class HasID a where
  type ID a
  getID :: a -> ID a

class Store r a where
  store :: r -> a -> IO ()

class Load r a where
  load :: r -> ID a -> IO (Maybe a)

class (Store r a, Load r a) => StoreAndLoad r a
instance (Store r a, Load r a) => StoreAndLoad r a

-- Here we use Repository unsaturated, so no constraint synonyms
data Has (c :: Type -> Constraint) where
  Has :: c t => t -> Has c


data Repository a where
  Repository :: StoreAndLoad r a => r -> Repository a


storeAndLoad :: HasID a => Repository a -> a -> IO (Maybe a)
storeAndLoad (Repository repo) a = do
  let i = getID a
  store repo a
  load repo i

-- We need to modify repo to have this actually make sense, so it is not obvious how to replicate
-- the pure state monad instance in this setting.
