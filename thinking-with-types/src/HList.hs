{-# LANGUAGE DataKinds            #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE GADTs                #-}
{-# LANGUAGE ScopedTypeVariables  #-}
{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE TypeOperators        #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE ConstraintKinds      #-}


module HList where

import           Data.Kind (Constraint, Type)


data HList (ts :: [Type]) where
  HNil :: HList '[]
  (:#) :: t -> HList ts -> HList (t ': ts)


hLength :: HList ts -> Int
hLength HNil      = 0
hLength (_ :# ts) = 1 + hLength ts

head :: HList (t ': ts) -> t
head (t :# _) = t

infixr 5 :#

instance (All Eq ts, All Ord ts) => Ord (HList ts) where
  HNil `compare` HNil = EQ
  (a :# as) `compare` (b :# bs) =
    let ordering = compare a b
    in case ordering of
      EQ -> compare as bs
      _ -> ordering


instance All Eq ts => Eq (HList ts) where
  HNil == HNil = True
  (a :# as) == (b :# bs) = a == b && as == bs


type family All (c :: Type -> Constraint) (ts :: [Type]) :: Constraint where
  All c '[] = ()
  All c (t ': ts) = (c t, All c ts)

join :: All Show ts => String -> HList ts -> String
join _ HNil = ""
join _ (t :# HNil) = show t
join sep (t :# ts) = show t ++ sep ++ join sep ts

instance All Show ts => Show (HList ts) where
  show ts = "[" ++ join "," ts ++ "]"

y :: HList '[Int, Bool, Int]
y = 1 :# True :# 2 :# HNil

x :: HList '[Int, Bool, Int]
x = 1 :# True :# 0 :# HNil


main :: IO ()
main = do
  print x
  print y



