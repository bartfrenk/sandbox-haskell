{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE UndecidableInstances  #-}
{-# LANGUAGE ConstraintKinds       #-}

module RepoConstraint where

import Control.Monad.State
import qualified Data.Map as M

class HasID a where
  type ID a
  getID :: a -> ID a

data User = User
  { userId :: Int
  , userName :: String
  }

class Store m a where
  store :: a -> m ()

class Load m a where
  load :: ID a -> m (Maybe a)


-- Slightly simpler than using superclasses, because we never use Repository unsaturated
type Repository m a = (Store m a, Load m a)


storeAndLoad :: (Monad m, Repository m a, HasID a) => a -> m (Maybe a)
storeAndLoad a = do
  let i = getID a
  store a
  load i

newtype Index a = Index { unIndex :: M.Map (ID a) a }

instance (HasID a, Ord (ID a)) => Load (State (Index a)) a where
  load i = gets (M.lookup i . unIndex)

instance (HasID a, Ord (ID a)) => Store (State (Index a)) a where
  store a = modify $ \(Index index) -> Index $ M.insert (getID a) a index
