{-# LANGUAGE LambdaCase #-}
module Pipes where


newtype Pipe a b r = Pipe { unPipe :: IO (PipeStep a b r) }

data PipeStep a b r
  = Pure r
  | Request (a -> Pipe a b r)
  | Respond a (Pipe a b r)

instance Functor (Pipe a b) where
  f `fmap` p = Pipe $ flip fmap (unPipe p) $ \case
      Pure r      -> Pure (f r)
      Request k   -> Request (fmap f . k)
      Respond a q -> Respond a (fmap f q)

instance Applicative (Pipe a b) where
  pure r = Pipe . return $ Pure r
  f <*> p = Pipe $ do
    fstep <- unPipe f
    case fstep of
      Pure r      -> unPipe (r `fmap` p)
      Respond a q -> return $ Respond a (q <*> p)
      Request k   -> return $ Request (\a -> k a <*> p)

instance Monad (Pipe a b) where


